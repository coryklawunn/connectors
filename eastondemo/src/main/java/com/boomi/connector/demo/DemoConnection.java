// Copyright (c) 2013 Boomi, Inc.
package com.boomi.connector.demo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import com.boomi.util.DOMUtil;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.URLUtil;

/**
 * Demo connection implementation.
 * 
 * @author Jeff Plater
 */
public class DemoConnection extends BaseConnection {
    /** default base url to use if none configured */
    private static final String DEFAULT_URL = "http://boomi-demo.appspot.com/rest";
    /** property which users can use to change the base url */
    private static final String URL_PROPERTY = "url";
    /** string representing an http POST method */
    private static final String POST_METHOD = "POST";
    /** string representing an http PUT method */
    private static final String PUT_METHOD = "PUT";
    /** string representing an http DELETE method */
    private static final String DELETE_METHOD = "DELETE";
    /** property name for the http content type header */
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    /** header property value for http content containing XML data */
    private static final String XML_CONTENT_TYPE = "application/xml";
    /** name of the metadata resource */
    private static final String METADATA_RESOURCE = "metadata";
    /** query params which indicate that update operation output should always be xml */
    private static final List<Map.Entry<String, String>> UPDATE_OUTPUT_PARAMS = Collections
            .unmodifiableList(new ArrayList<Map.Entry<String, String>>(Collections.singletonMap("type", "xml")
                    .entrySet()));

    private final String _baseUrl;

    public DemoConnection(BrowseContext context) {
        super(context);
        _baseUrl = getBaseUrl(context.getConnectionProperties());
    }

    /**
     * Retrieves the list of available objects types or, if objectTypeId is specified, the definition of the given type
     * 
     * @param objectTypeId id of the object type to get the object definition for or null to get all available objects
     * @return response from calling the metadata service
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public Document getMetadata(String objectTypeId) throws IOException, SAXException {
        URL url = objectTypeId != null ? buildUrl(_baseUrl, METADATA_RESOURCE, objectTypeId) : buildUrl(_baseUrl,
                METADATA_RESOURCE);
        return parse(url.openStream());
    }

    /**
     * Fetches the given object 
     * 
     * @param objectType id of the object type
     * @param objectId id of the object to be retrieved
     * @return response from the demo service
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public DemoResponse doGet(String objectType, String objectId) throws IOException
    {
        URL url = buildUrl(_baseUrl, objectType, objectId);
        return new DemoResponse((HttpURLConnection) url.openConnection());
    }

    /**
     * Executes a query using the given query terms
     * 
     * @param queryTerms query filter terms for the demo service
     * @param objectType id of the object type
     * @return response from the demo service
     * @throws IOException
     */
    public DemoResponse doQuery(List<Map.Entry<String, String>> queryTerms, String objectType) throws IOException
    {
        URL url = buildUrl(queryTerms, _baseUrl, objectType);
        return new DemoResponse((HttpURLConnection) url.openConnection());
    }

    /**
     * Updates the given objects
     * 
     * @param replace whether or not to do complete replacement
     * @param objectType id of the object type
     * @param inputBatch batch of objects to update
     * @return response from the demo service
     * @throws IOException
     * @throws XMLStreamException
     */
    public DemoResponse doUpdate(boolean replace, String objectType, List<ObjectData> inputBatch) throws IOException,
            XMLStreamException {
        // construct batch request url for this object
        URL url = buildUrl(UPDATE_OUTPUT_PARAMS, _baseUrl, objectType);

        // POST (partial update) or PUT (complete replacement) the content to
        // the update url
        String requestMethod = (replace ? PUT_METHOD : POST_METHOD);
        OutputStream out = null;
        DemoRequestJoiner reqJoiner = null;
        try {

            HttpURLConnection conn = prepareSend(url, requestMethod, XML_CONTENT_TYPE);
            out = conn.getOutputStream();

            // join the current batch into one output xml document, wrapped with
            // a "list" element
            reqJoiner = new DemoRequestJoiner(out);
            reqJoiner.writeAll(inputBatch);
            reqJoiner.close();
            out.close();

            return new DemoResponse(conn);
        }
        finally {
            IOUtil.closeQuietly(reqJoiner, out);
        }
    }

    /**
     * Creates the given object
     * 
     * @param objectType id of the object type
     * @param data object to create
     * @return response from demo service
     * @throws IOException
     */
    public DemoResponse doCreate(String objectType, InputStream data) throws IOException {
        return new DemoResponse(send(buildUrl(UPDATE_OUTPUT_PARAMS, _baseUrl, objectType), POST_METHOD,
                XML_CONTENT_TYPE, data));
    }

    /**
     * Deletes the object with the given id 
     * 
     * @param objectType id of the object type
     * @param objectId id of the object to delete
     * @return response from the demo service
     * @throws IOException
     */
    public DemoResponse doDelete(String objectType, String objectId) throws IOException {
        // construct request url for this object
        URL url = buildUrl(_baseUrl, objectType, objectId);

        // make DELETE request to the deletion url
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(DELETE_METHOD);

        return new DemoResponse(conn);
    }

    private static String getBaseUrl(PropertyMap props) {
        return props.getProperty(URL_PROPERTY, DEFAULT_URL);
    }

    private static Document parse(InputStream input) throws SAXException, IOException {
        try {

            return DOMUtil.newDocumentBuilderNS().parse(input);
        }
        finally {
            IOUtil.closeQuietly(input);
        }
    }

    private static URL buildUrl(String... components) throws IOException {
        return buildUrl((List<Map.Entry<String, String>>) null, components);
    }

    private static URL buildUrl(List<Map.Entry<String, String>> params, String... components) throws IOException {
        // generate url w/ path
        return URLUtil.makeUrl(params, (Object[]) components);
    }

    private static HttpURLConnection send(URL url, String requestMethod, String contentType, InputStream data)
            throws IOException {
        try {
            HttpURLConnection conn = prepareSend(url, requestMethod, contentType);

            OutputStream out = conn.getOutputStream();
            try {
                StreamUtil.copy(data, out);
            }
            finally {
                out.close();
            }

            return conn;
        }
        finally {
            IOUtil.closeQuietly(data);
        }
    }

    private static HttpURLConnection prepareSend(URL url, String requestMethod, String contentType) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(requestMethod);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestProperty(CONTENT_TYPE_HEADER, contentType);
        return conn;
    }

}
