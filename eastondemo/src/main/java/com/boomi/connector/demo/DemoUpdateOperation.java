// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.RequestUtil;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;

/**
 * Demo UPDATE operation implementation. This implementation shows an example of
 * batch object processing. See {@link DemoCreateOperation} for a simple,
 * non-batched example.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoUpdateOperation extends BaseUpdateOperation {
    /** property which users can use specify complete replacement */
    protected static final String REPLACE_PROPERTY = "replace";
    /** default value for the replace property */
    private static final boolean DEFAULT_REPLACE = false;

    private final boolean _replace;

    public DemoUpdateOperation(DemoConnection conn) {
        super(conn);
        _replace = getContext().getOperationProperties().getBooleanProperty(REPLACE_PROPERTY, DEFAULT_REPLACE);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        String objectType = getContext().getObjectTypeId();

        // group the inputs into batches for processing
        for (List<ObjectData> inputBatch : RequestUtil.pageIterable(request, getContext().getConfig())) {
            DemoResponseSplitter respSplitter = null;
            try {
                // make the update call
                DemoResponse resp = getConnection().doUpdate(_replace, objectType, inputBatch);

                if (resp.getStatus() != OperationStatus.SUCCESS) {
                    // just dump the failure result for the current batch
                    ResponseUtil.addEmptyResults(response, inputBatch, resp.getStatus(),
                            resp.getResponseCodeAsString(), resp.getResponseMessage());
                    continue;
                }

                // split "list" result document into multiple payloads and
                // process each as a result for the corresponding input
                respSplitter = new DemoResponseSplitter(resp.getResponse());

                Iterator<ObjectData> inputIter = inputBatch.iterator();
                for (Payload p : respSplitter) {
                    ObjectData input = inputIter.next();
                    response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                            resp.getResponseMessage(), p);
                }

                // if there are any inputs without outputs, mark them as errored
                while (inputIter.hasNext()) {
                    ResponseUtil.addEmptyFailure(response, inputIter.next(),
                            String.valueOf(HttpURLConnection.HTTP_INTERNAL_ERROR));
                }

            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailures(response, inputBatch, e);
            }
            finally {
                IOUtil.closeQuietly(respSplitter);
            }
        }
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }

}
