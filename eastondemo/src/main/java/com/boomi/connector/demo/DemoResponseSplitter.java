// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.boomi.connector.util.xml.XMLSplitter;

/**
 * XMLSplitter for multiple object responses from the demo service.  Splits the xml document with a "list" root
 * element into multiple objects.  Also stores any returned next "offset" attribute from the response.
 *
 * @author James Ahlborn
 */
public class DemoResponseSplitter extends XMLSplitter
{
    private static final QName OFFSET_PARAM_QNAME = new QName(DemoConnector.OFFSET_PARAM);
    
    private String _nextOffset;
    
    public DemoResponseSplitter(InputStream in)
        throws XMLStreamException
    {
        super(createInputFactory().createXMLEventReader(in));
    }

    /**
     * Returns any next "offset" attribute included in the response.  Not valid until the response has been consumed.
     */
    public String getNextOffset()
    {
        return _nextOffset;
    }
    
    @Override
    protected XMLEvent findNextObjectStart(boolean isFirst) throws XMLStreamException
    {
        if(isFirst) {
            
            // skip past the first start-element, which should be the "list" wrapper element
            StartElement event = findNextElementStart(getReader());
            if(!DemoConnector.LIST_EL_NAME.equals(event.getName().getLocalPart())) {
                throw new IllegalStateException("Unexpected document element " + event.getName());
            }

            // while we are here, grab the "offset" attribute, if it exists
            Attribute attr = event.getAttributeByName(OFFSET_PARAM_QNAME);
            if(attr != null) {
                _nextOffset = attr.getValue();
            }
        }

        // skip to the next start-element event
        return findNextElementStart(getReader());
    }    
}
