// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.io.InputStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.GroupingOperator;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

/**
 * Demo QUERY operation implementation.
 *
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoQueryOperation extends BaseQueryOperation
{

    /** Enum representing the supported query operations (these names match the ids specified in the connector
        descriptor).  Each operation has a prefix field which is used when sending the query filter to the demo
        service. */
    private enum QueryOp {
        EQUALS("feq_"),
        NOT_EQUALS("fne_"),
        GREATER_THAN("fgt_"),
        LESS_THAN("flt_"),
        GREATER_THAN_OR_EQUALS("fge_"),
        LESS_THAN_OR_EQUALS("fle_"),
        IN_LIST("fin_");

        private final String _prefix;

        private QueryOp(String prefix) {
            _prefix = prefix;
        }

        
        /**
         * Returns the prefix to use for this operation with the demo service.
         */
        public String getPrefix() {
            return _prefix;
        }
    }
    
    public DemoQueryOperation(DemoConnection conn) {
        super(conn);
    }

    @Override
    protected void executeQuery(QueryRequest request, OperationResponse response) {
        FilterData input = request.getFilter();
        try {

            // convert the sdk filter into url query filter pairs
            List<Map.Entry<String,String>> baseQueryTerms = constructQueryTerms(input.getFilter());
            
            // grab the object type from the context
            String objectType = getContext().getObjectTypeId();
            
            // process all the results, handling batching
            String nextOffset = null;
            do {
                
                List<Map.Entry<String,String>> queryTerms; 
                
                if (nextOffset != null) {
                    // add the paging information to the query params
                    queryTerms = new ArrayList<Map.Entry<String,String>>(baseQueryTerms);
                    queryTerms.add(new AbstractMap.SimpleEntry<String, String>(DemoConnector.OFFSET_PARAM, nextOffset));
                } else {
                    // first time through, no paging informatin to add
                    queryTerms = baseQueryTerms;
                }
                
                // make the query request
                DemoResponse resp = getConnection().doQuery(queryTerms, objectType);
                
                if (resp.getStatus() != OperationStatus.SUCCESS) {
                    // just dump the result and bailout, nothing more we can do
                    InputStream obj = null;
                    try {
                        obj = resp.getResponse();
                        if (obj != null) {

                            response.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                    resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                        }
                        else {
                            response.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                    resp.getResponseMessage(), null);
                        }
                    }
                    finally {
                        IOUtil.closeQuietly(obj);
                    }
                    break;
                }

                DemoResponseSplitter respSplitter = null;
                try {

                    // split "list" result document into multiple payloads and process each as a partial result
                    respSplitter = new DemoResponseSplitter(resp.getResponse());

                    for(Payload p : respSplitter) {
                        ResponseUtil.addPartialSuccess(response, input, resp.getResponseCodeAsString(), p);
                    }

                    nextOffset = respSplitter.getNextOffset();
                    
                } finally {
                    IOUtil.closeQuietly(respSplitter);
                }
                
            } while(!StringUtil.isBlank(nextOffset));

            // indicate that we are finished adding all the results
            response.finishPartialResult(input);
            
        } catch(Exception e) {
            ResponseUtil.addExceptionFailure(response, input, e);
        }
    }

    /**
     * Constructs a list of query filter terms from the given filter, may be {@code null} or empty.
     *
     * @param filter query filter from which to construct the terms
     *
     * @return collection of query filter terms for the demo service
     */
    static List<Map.Entry<String,String>> constructQueryTerms(QueryFilter filter) {
        if((filter == null) || (filter.getExpression() == null)) {
            // no filter given, (this is equivalent to "select all")
            return null;
        }

        List<Map.Entry<String,String>> terms = new ArrayList<Map.Entry<String,String>>();

        // see if base expression is a single expression or a grouping expression
        Expression baseExpr = filter.getExpression();
        if(baseExpr instanceof SimpleExpression) {
            
            // base expression is a single simple expression
            terms.add(constructSimpleExpression((SimpleExpression)baseExpr));
            
        } else {

            // handle single level of grouped expressions
            GroupingExpression groupExpr = (GroupingExpression)baseExpr;

            // we only support "AND" groupings
            if(groupExpr.getOperator() != GroupingOperator.AND) {
                throw new IllegalStateException("Invalid grouping operator " + groupExpr.getOperator());
            }

            // parse all the simple expressions in the group
            for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                if(!(nestedExpr instanceof SimpleExpression)) {
                    throw new IllegalStateException("Only one level of grouping supported");
                }
                terms.add(constructSimpleExpression((SimpleExpression)nestedExpr));
            }
        }
        
        return terms;
    }

    /**
     * Returns a url query term (key, value pair) constructed from the given SimpleExpression.
     *
     * @param expr the simple expression from which to construct the term
     *
     * @return url query filter term for the demo service
     */
    static Map.Entry<String,String> constructSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
        String propName = expr.getProperty();

        // translate the operation id into one of our supported operations
        QueryOp queryOp = QueryOp.valueOf(expr.getOperator());

        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) {
            throw new IllegalStateException("Unexpected number of arguments for operation " + queryOp + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");
        }

        // this is the single operation argument
        String queryValue = expr.getArguments().get(0);

        // combine the property name and operation into the query filter key
        String queryKey = queryOp.getPrefix() + propName;

        return new AbstractMap.SimpleEntry<String, String>(queryKey, queryValue);
    }
    
    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }
    
}
