// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.io.InputStream;
import java.net.HttpURLConnection;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.util.BaseGetOperation;
import com.boomi.util.IOUtil;

/**
 * Demo GET operation implementation.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoGetOperation extends BaseGetOperation {

    public DemoGetOperation(DemoConnection conn) {
        super(conn);
    }

    @Override
    protected void executeGet(GetRequest request, OperationResponse response) {
        ObjectIdData input = request.getObjectId();
        try {
            // make GET request for the object of the given type and id
            DemoResponse resp = getConnection().doGet(getContext().getObjectTypeId(), input.getObjectId());

            if (resp.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                // A GET request that returns 404 "not found" is considered a success.
                response.addEmptyResult(input, OperationStatus.SUCCESS, resp.getResponseCodeAsString(),
                        resp.getResponseMessage());
            }
            else {
                // dump the results into the response
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {
                        response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                        response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage());
                    }
                }
                finally {
                    IOUtil.closeQuietly(obj);
                }
            }

        }
        catch (Exception e) {
            ResponseUtil.addExceptionFailure(response, input, e);
        }
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }
}
