// Copyright (c) 2019 Dell Boomi, Inc.
package com.boomi.connector.demo;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseConnection;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * Implementation of custom Execute Operation for Echo Fields
 */

public class DemoEchoOperation extends BaseUpdateOperation {

    protected DemoEchoOperation (BaseConnection conn) {
        super(conn);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        DemoEchoResponse echoResponse = new DemoEchoResponse(getContext());
        ResponseUtil.addCombinedSuccess(response, request, "200", echoResponse.getPayload());
    }

}
