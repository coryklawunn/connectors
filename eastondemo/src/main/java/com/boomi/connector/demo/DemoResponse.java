// Copyright (c) 2013 Boomi, Inc.
package com.boomi.connector.demo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import com.boomi.connector.api.OperationStatus;

/**
 * Simple object that wraps the response from the demo service
 *  
 * @author jplater
 *
 */
public class DemoResponse {

    private final HttpURLConnection _conn;
    private final int _responseCode;
    private final String _responseMsg;

    public DemoResponse(HttpURLConnection conn) throws IOException {
        _conn = conn;
        _responseCode = conn.getResponseCode();
        _responseMsg = conn.getResponseMessage();
    }

    public InputStream getResponse() throws IOException {
        try {
            return _conn.getInputStream();
        }
        catch (IOException e) {
            if (OperationStatus.SUCCESS == getStatus()) {
                // bummer, that's not good
                throw e;
            }
            return _conn.getErrorStream();
        }
    }

    public int getResponseCode() {
        return _responseCode;
    }

    public String getResponseCodeAsString() {
        return String.valueOf(_responseCode);
    }

    public String getResponseMessage() {
        return _responseMsg;
    }

    /**
     * Returns the OperationStatus for the given http code.
     * 
     * @return SUCCESS if the code indicates success, FAILURE otherwise
     */
    public OperationStatus getStatus() {
        // success: 200 <= code < 300
        if (_responseCode >= HttpURLConnection.HTTP_OK && _responseCode < HttpURLConnection.HTTP_MULT_CHOICE) {
            return OperationStatus.SUCCESS;
        }
        else if (_responseCode >= HttpURLConnection.HTTP_MULT_CHOICE
                && _responseCode < HttpURLConnection.HTTP_INTERNAL_ERROR) {
            return OperationStatus.APPLICATION_ERROR;
        }
        else {
            return OperationStatus.FAILURE;
        }
    }
}
