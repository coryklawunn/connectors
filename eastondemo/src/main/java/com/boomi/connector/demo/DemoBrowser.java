// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.util.Collection;
import java.util.Map;

import com.boomi.connector.api.ConnectionTester;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;

/**
 * Demo browser implementation.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoBrowser extends BaseBrowser implements ConnectionTester {

    private static final String TYPE_ELEMENT = "type";

    public DemoBrowser(DemoConnection conn) {
        super(conn);
    }

    @Override
    public ObjectTypes getObjectTypes() {
        try {
            // parse the returned XML data and generate the ObjectTypes
            // collection
            Document typeDoc = getConnection().getMetadata(null);
            NodeList typeList = typeDoc.getElementsByTagName(TYPE_ELEMENT);
            ObjectTypes types = new ObjectTypes();
            for (int i = 0; i < typeList.getLength(); ++i) {
                Element typeEl = (Element) typeList.item(i);
                String typeName = typeEl.getTextContent().trim();
                ObjectType type = new ObjectType();
                type.setId(typeName);
                types.getTypes().add(type);
            }

            return types;

        }
        catch (Exception e) {
            throw new ConnectorException(e);
        }
    }

    @Override
    public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
        try {
            // parse the returned XML Schema document and construct an
            // ObjectDefinition
            Document defDoc = getConnection().getMetadata(objectTypeId);
            ObjectDefinitions defs = new ObjectDefinitions();
            ObjectDefinition def = new ObjectDefinition();
            def.setSchema(defDoc.getDocumentElement());
            def.setElementName(objectTypeId);
            defs.getDefinitions().add(def);

            return defs;

        }
        catch (Exception e) {
            throw new ConnectorException(e);
        }
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }

    @Override
    public void testConnection() {
        boolean forceTestConnectionFailure = getConnection().getContext()
                .getConnectionProperties().getBooleanProperty("forceTestConnectionFailure", false);
        boolean testUsingGet = getConnection().getContext()
                .getConnectionProperties().getBooleanProperty("testUsingGet", false);
        boolean testImmutability = getConnection().getContext()
                .getConnectionProperties().getBooleanProperty("testMapImmutability", false);
        boolean pullValuesForField2 = getConnection().getContext()
                .getConnectionProperties().getBooleanProperty("testField2Values", false);

        Map<String, String> testConnectionCustomPropertyField = null;

        if(testUsingGet) {
           	if(pullValuesForField2 == false) {
                //Get the property a different way
                testConnectionCustomPropertyField = (Map<String, String>) getConnection().getContext()
                        .getConnectionProperties().get("testConnectionCustomPropertyField");
            	} else {
                    testConnectionCustomPropertyField = (Map<String, String>) getConnection().getContext()
                            .getConnectionProperties().get("testConnectionCustomPropertyField2");
            	}
            } else if (pullValuesForField2 == true){
                //Get the property the right way
                testConnectionCustomPropertyField = getConnection().getContext()
                        .getConnectionProperties().getCustomProperties("testConnectionCustomPropertyField2");
            } else {
                testConnectionCustomPropertyField = getConnection().getContext()
                        .getConnectionProperties().getCustomProperties("testConnectionCustomPropertyField");
            }

        //the value should never be null
        if(testConnectionCustomPropertyField == null) {
            throw new ConnectorException("The field is null, and it shouldn't be.");
        }

        for(Map.Entry<String, String> keyValuePair : testConnectionCustomPropertyField.entrySet()) {
            if( (keyValuePair.getKey() == null) || (keyValuePair.getValue() == null) ) {
                throw new ConnectorException("The key or value of a keyvalue pair was null : Key: " + keyValuePair.getKey()
                + ", Value: " + keyValuePair.getValue());
            }
        }

        //this will test to make sure we are getting an immutable map and fail if we don't get one.
        if(testImmutability) {
            try {
                testConnectionCustomPropertyField.put("newProperty", "property");
                if(testConnectionCustomPropertyField.containsKey("newProperty")) {
                    throw new ConnectorException("The map should not have allowed us to modify its contents. " +
                            "The custom property value return is of type: " +
                            testConnectionCustomPropertyField.getClass().toString());
                }
            } catch (UnsupportedOperationException uoe) {
                //Expected exception, move along nothing to see here.
            }

        }

        //the customproperties field test to make sure the size is exactly 4 coming into the connector
        if(testConnectionCustomPropertyField.size() != 4) {
            throw new ConnectorException("The custom property test connection field must contain exactly 4 entries. " +
                    "There are " + testConnectionCustomPropertyField.size() + " entries.");
        }


        /**
         *
         * Place other assertions here. Basically this test connection will FAIL
         * if you throw an exception. So you can throw a new connector exception
         * anytime your test data gets to your connector incorrectly.
         *
         */


        //print the map if you want in the failure report
        if(forceTestConnectionFailure) {
            throw new ConnectorException("Forced failure. testConnectionCustomPropertyField value: \n" +
                    testConnectionCustomPropertyField.toString());
        }
    }
}
