// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.io.OutputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import com.boomi.connector.util.xml.ElementWrapperEventGenerator;
import com.boomi.connector.util.xml.XMLJoiner;

/**
 * XMLJoiner for generating multiple object requests for the demo service.  Joins multiple xml objects into one
 * document, wrapping them with a "list" root element.
 *
 * @author James Ahlborn
 */
public class DemoRequestJoiner extends XMLJoiner 
{
    private static final QName LIST_EL_QNAME = new QName(DemoConnector.LIST_EL_NAME);

    private ElementWrapperEventGenerator _generator;
    
    public DemoRequestJoiner(OutputStream out) throws XMLStreamException
    {
        super(createOutputFactory().createXMLEventWriter(out));
    }

    @Override
    protected void startDocument() throws XMLStreamException
    {
        super.startDocument();

        if(_generator == null) {
            _generator = new ElementWrapperEventGenerator(getEventFactory())
                .addWrapperElement(LIST_EL_QNAME);
        }

        _generator.addStartEvents(getWriter());
    }
    @Override
    protected void endDocument() throws XMLStreamException
    {
        _generator.addEndEvents(getWriter());
    }
}
