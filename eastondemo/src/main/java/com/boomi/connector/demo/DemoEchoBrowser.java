// Copyright (c) 2019 Dell Boomi, Inc.
package com.boomi.connector.demo;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.ClassUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.LogUtil;
import com.boomi.util.StreamUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Browser implementation to generate object types and definitions for the
 * Echo Fields Operation
 */

public class DemoEchoBrowser extends BaseBrowser {

    private static final String ECHO_OBJECT_ID = "Echo";
    private static final String ECHO_OBJECT_LABEL = "Operation Fields";
    private static final String ECHO_SCHEMA_LOCATION = "/schemas/echo-operation-schema.json";
    private static final String ECHO_SCHEMA_CHARSET = "UTF-8";
    private static final Logger LOGGER = LogUtil.getLogger(DemoEchoBrowser.class);


    public DemoEchoBrowser(DemoConnection conn) {
        super(conn);
    }

    @Override
    public ObjectTypes getObjectTypes() {
        ObjectTypes types = new ObjectTypes();
        ObjectType type = new ObjectType();
        type.setId(ECHO_OBJECT_ID);
        type.setLabel(ECHO_OBJECT_LABEL);
        types.getTypes().add(type);
        return types;
    }

    @Override
    public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {

        ObjectDefinitions defs = new ObjectDefinitions();

        // The first ObjectDefinition added is the input def
        // Operation doesn't need input so use a ObjectDefinition with inputType NONE
        ObjectDefinition blankInputDef = new ObjectDefinition();
        blankInputDef.setInputType(ContentType.NONE);
        defs.getDefinitions().add(blankInputDef);

        // The Second ObjectDefinition added is the output def
        // Output type is JSON and the schema is a valid JSON schema string
        ObjectDefinition def = new ObjectDefinition();
        def.setOutputType(ContentType.JSON);
        def.setJsonSchema(getJsonSchema());

        // When using JSON schemas the element name must be a valid JSON pointer according to the RFC6901 spec
        def.setElementName("");

        // Its possible to set cookies on a ObjectDefinition at browse time if there is info
        // that needs to be saved for later. It is a good idea to store this as some structured
        // format in case more data needs to be added in the future
        DemoEchoCookie cookie = DemoEchoCookie.withCurrentTime();
        def.setCookie(cookie.getAsJson());

        //Printing all properties in the property map and avoiding a NPE for toString().
        LOGGER.warning("Browse Properties:");
        for(Map.Entry<String, Object> property : getContext().getOperationProperties().entrySet()) {
            LOGGER.warning(property.getKey() + ":" +
                    ((property.getValue() != null) ? property.getValue().toString() : property.getValue()));
        }

        LOGGER.warning("Browse Custom Properties:");
        Map<String, String> properties =
                getContext().getOperationProperties().getCustomProperties("customPropertyField");

        //Printing the values on a browse for an operation property
        for(Map.Entry<String, String> property : properties.entrySet()) {
            LOGGER.warning(property.getKey() + ":" + property.getValue());
        }

        LOGGER.warning("Connection Custom Properties:");
        Map<String, String> connectionProperties =
                getContext().getConnectionProperties().getCustomProperties("customPropertyConnection2");

        //Printing the values on a browse for an connection property
        for(Map.Entry<String, String> property : connectionProperties.entrySet()) {
            LOGGER.warning(property.getKey() + ":" + property.getValue());
        }

        defs.getDefinitions().add(def);
        return defs;
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }

    private static String getJsonSchema(){
        String schema;
        InputStream is = ClassUtil.getResourceAsStream(ECHO_SCHEMA_LOCATION);
        try {
            schema = StreamUtil.toString(is, ECHO_SCHEMA_CHARSET);
        } catch (IOException ex) {
            throw new ConnectorException("Error reading schema", ex);
        } finally {
            IOUtil.closeQuietly(is);
        }
        return schema;
    }

}
