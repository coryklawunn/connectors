// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.util.BaseDeleteOperation;

/**
 * Demo DELETE operation implementation.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoDeleteOperation extends BaseDeleteOperation {

    public DemoDeleteOperation(DemoConnection conn) {
        super(conn);
    }

    @Override
    protected void executeDelete(DeleteRequest request, OperationResponse response) {
        String objectType = getContext().getObjectTypeId();

        for (ObjectIdData input : request) {
            try {
                DemoResponse resp = getConnection().doDelete(objectType, input.getObjectId());
                // just return status values (no content)
                response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                        resp.getResponseMessage());
            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }

}
