// Copyright (c) 2019 Dell Boomi, Inc.
package com.boomi.connector.demo;

import com.boomi.connector.api.ConnectorException;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Simple data structure for storing and accessing browse cookie data
 */

public class DemoEchoCookie {

    // The Stock ObjectMapper created in jackson can not be used in the boomi cloud for security reasons
    // disable CAN_OVERRIDE_ACCESS_MODIFIERS to make it security compliant
    private static final ObjectMapper MAPPER = new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);

    @JsonProperty
    private String importTime;

    public DemoEchoCookie() {
        // Default constructor for JSON deserialization
    }

    public DemoEchoCookie(String importTime) {
        this.importTime = importTime;
    }

    @JsonGetter
    public String getImportTime() {
        return importTime;
    }

    @JsonSetter
    public void setImportTime(String importTime) {
        this.importTime = importTime;
    }

    /**
     * Return this data structure as a JSON string
     * @return JSON representation of this data
     */
    @JsonIgnore
    public String getAsJson() {
        try {
            return MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            throw new ConnectorException("Error writing cookie", ex);
        }
    }

    /**
     * Create instance from JSON string
     * @param value JSON string representing containing data
     * @return Cookie containing data from the JSON string.  Blank Cookie if value is null
     */
    public static DemoEchoCookie parseJson(String value) {

        if (value == null) {
            return new DemoEchoCookie();
        }

        try {
            return MAPPER.readValue(value, DemoEchoCookie.class);
        } catch (IOException ex) {
            throw new ConnectorException("Error parsing cookie", ex);
        }
    }

    /**
     * Create new instance using the current time
     * @return Cookie with import time set to current time
     */
    public static DemoEchoCookie withCurrentTime(){
        return new DemoEchoCookie(currentTime());
    }

    private static String currentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm Z");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(new Date());
    }
}
