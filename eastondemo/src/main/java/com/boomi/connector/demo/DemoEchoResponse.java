// Copyright (c) 2019 Dell Boomi, Inc.
package com.boomi.connector.demo;

import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PropertyMap;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Simple class to generate JSON payload for the Echo Fields Operation
 */

public class DemoEchoResponse {

    private static final String STRING_OP_PROP = "stringField";
    private static final String INTEGER_OP_PROP = "integerField";
    private static final String PASSWORD_OP_PROP = "passwordField";
    private static final String BOOLEAN_OP_PROP = "booleanField";
    private static final String LIST_OP_PROP = "listField";
    private static final String RADIO_OP_PROP = "radioField";
    private static final String CUSTOM_PROPERTY_OP_PROP = "customPropertyField";
    private static final String CUSTOM_PROPERTY_CON_PROP = "customPropertyConnection2";

    @JsonProperty("integerField")
    private Long _integerField;
    @JsonProperty("stringField")
    private String _stringField;
    @JsonProperty("passwordField")
    private String _passwordField;
    @JsonProperty("booleanField")
    private boolean _booleanField;
    @JsonProperty("listField")
    private String _listField;
    @JsonProperty("radioField")
    private Long _radioField;
    @JsonProperty("cookie")
    private DemoEchoCookie _cookie;
    @JsonProperty("customPropertiesConnectionField")
    private List<CustomProperties> _customPropertiesConnectionField;
    @JsonProperty("customPropertiesOperationField")
    private List<CustomProperties> _customPropertiesOperationField;

    private final boolean _testUsingGet;

    public DemoEchoResponse (OperationContext context) {
        PropertyMap properties = context.getOperationProperties();
        _integerField = properties.getLongProperty(INTEGER_OP_PROP);
        _stringField = properties.getProperty(STRING_OP_PROP);
        _passwordField = properties.getProperty(PASSWORD_OP_PROP);
        _booleanField = properties.getBooleanProperty(BOOLEAN_OP_PROP);
        _listField = properties.getProperty(LIST_OP_PROP);
        _radioField = properties.getLongProperty(RADIO_OP_PROP);
        _cookie = DemoEchoCookie.parseJson(context.getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT));
        _testUsingGet = context.getConnectionProperties().getBooleanProperty("testUsingGet", false);
        _customPropertiesConnectionField = createCustomPropertiesList(context.getConnectionProperties(), CUSTOM_PROPERTY_CON_PROP);
        _customPropertiesOperationField = createCustomPropertiesList(properties, CUSTOM_PROPERTY_OP_PROP);

    }

    /**
     * Convert the response into a JSON payload to add to an Operation Result
     * @return Payload containing the json data
     */
    @JsonIgnore
    public Payload getPayload(){
        return JsonPayloadUtil.toPayload(this);
    }

    @JsonGetter("integerField")
    public Long getIntegerField(){
        return _integerField;
    }
    @JsonGetter("stringField")
    public String getStringField() {
        return _stringField;
    }
    @JsonGetter("passwordField")
    public String getPasswordField() {
        return _passwordField;
    }
    @JsonGetter("booleanField")
    public boolean getBooleanField() {
        return _booleanField;
    }
    @JsonGetter("listField")
    public String getListField() {
        return _listField;
    }
    @JsonGetter("radioField")
    public Long getRadioField() {
        return _radioField;
    }
    @JsonGetter("cookie")
    public DemoEchoCookie getCookie() {
        return _cookie;
    }
    @JsonGetter("customPropertiesConnectionField")
    public List<CustomProperties> getCustomPropertyConnectionField() {
        return _customPropertiesConnectionField;
    }
    @JsonGetter("customPropertiesOperationField")
    public List<CustomProperties> getCustomPropertyOperationField() {
        return _customPropertiesOperationField;
    }

    private List<CustomProperties> createCustomPropertiesList(PropertyMap properties, String key) {
        Map<String, String> customPropertyValue;
        if(_testUsingGet) {
            customPropertyValue = (Map<String, String>) properties.get(key);
        } else {
            customPropertyValue = properties.getCustomProperties(key);
        }

        List<CustomProperties> parsedProperties = new ArrayList<CustomProperties>();
        for(Map.Entry<String, String> property : customPropertyValue.entrySet()) {
            parsedProperties.add(new CustomProperties(property.getKey(), property.getValue()));
        }

        return parsedProperties;
    }

    public class CustomProperties {

        @JsonProperty("key")
        public String _key;
        @JsonProperty("value")
        public String _value;

        public CustomProperties(String key, String value) {
            _key = key;
            _value = value;
        }

        @JsonGetter("key")
        public String getKey() {
            return _key;
        }
        @JsonGetter("value")
        public String getValue() {
            return _value;
        }
    }
}
