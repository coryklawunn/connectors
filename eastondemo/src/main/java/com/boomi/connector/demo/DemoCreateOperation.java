// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import java.io.InputStream;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;

/**
 * Demo CREATE operation implementation. This implementation shows an example of
 * a simple, non-batched object processing. See {@link DemoUpdateOperation} for
 * a more complex, batched example.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoCreateOperation extends BaseUpdateOperation {

    public DemoCreateOperation(DemoConnection conn) {
        super(conn);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        String objectType = getContext().getObjectTypeId();

        for (ObjectData input : request) {

            try {
                // POST the content to the create url
                DemoResponse resp = getConnection().doCreate(objectType, input.getData());

                // dump the results into the response
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {
                        response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                        response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage());
                    }

                }
                finally {
                    IOUtil.closeQuietly(obj);
                }

            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
    }

    @Override
    public DemoConnection getConnection() {
        return (DemoConnection) super.getConnection();
    }

}
