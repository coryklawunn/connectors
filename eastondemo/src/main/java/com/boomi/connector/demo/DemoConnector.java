// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;
import com.boomi.util.StringUtil;

/**
 * Demo Boomi Connector SDK connector implementation. Utilizes the <a
 * href="http://boomi-demo.appspot.com/">Boomi Google AppEngine demo
 * service</a>.
 * 
 * @author James Ahlborn
 * @author Jeff Plater
 */
public class DemoConnector extends BaseConnector {
    /** query parameter for getting more results */
    protected static final String OFFSET_PARAM = "offset";

    /** name of wrapper element for multiple object request/response */
    protected static final String LIST_EL_NAME = "list";

    /** Name of custom operation that requires special handling */
    public static final String ECHO_OPERATION_TYPE = "ECHO";

    public DemoConnector() {
    }

    @Override
    public Browser createBrowser(BrowseContext context) {
        if (getCustomOperationType(context).equals(ECHO_OPERATION_TYPE)){
            return new DemoEchoBrowser(createConnection(context));
        }
        return new DemoBrowser(createConnection(context));
    }

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new DemoGetOperation(createConnection(context));
    }

    @Override
    protected Operation createCreateOperation(OperationContext context) {
        return new DemoCreateOperation(createConnection(context));
    }

    @Override
    protected Operation createUpdateOperation(OperationContext context) {
        return new DemoUpdateOperation(createConnection(context));
    }

    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        return new DemoDeleteOperation(createConnection(context));
    }

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new DemoQueryOperation(createConnection(context));
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        if (getCustomOperationType(context).equals(ECHO_OPERATION_TYPE)) {
            return new DemoEchoOperation(createConnection(context));
        }
        return super.createExecuteOperation(context);
    }

    private DemoConnection createConnection(BrowseContext context) {
        return new DemoConnection(context);
    }

    private String getCustomOperationType(BrowseContext context){
        return StringUtil.defaultIfBlank(context.getCustomOperationType(), "");
    }
}
