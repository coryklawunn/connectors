// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.boomi.util.DOMUtil;
import com.boomi.util.XMLUtil;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;
import com.boomi.util.StringUtil;

/**
 * @author James Ahlborn
 */
public class DemoOperationTest 
{
    // google app returns microseconds, so pad millis
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'000'";
    private static final TimeZone GMT = TimeZone.getTimeZone("GMT");

    private static final String TEST_TYPE = "Greeting";
    private static final String TEST_CONTENT1 = "this is some test content";
    private static final String TEST_CONTENT2 = "this is some modified test content";
    private static final String TEST_AUTHOR = "demoTest";

    private static final String KEY_EL_NAME = "key";
    
    private static final DocumentBuilderFactory DBF = DOMUtil.newDocumentBuilderFactoryNS();


    @Test
    public void testOperations() throws Exception
    {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        setOperationContext(tester, OperationType.CREATE);

        String timeStr = currentTime();

        // create new Greeting instance
        List<SimpleOperationResult> results =
            tester.executeCreateOperation(
                    Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                       createGreeting(timeStr, null, TEST_CONTENT1, TEST_AUTHOR))));
        assertEquals(1, results.size());
        SimpleOperationResult result = results.get(0);
        assertTrue(result.getStatus() == OperationStatus.SUCCESS);
        assertEquals(1, result.getPayloads().size());

        // grab new instance key from results
        String key = getKey(result);
        byte[] keyBytes = createKeyResult(key);

        byte[] greetingBytes = createGreeting(timeStr, key, TEST_CONTENT1, TEST_AUTHOR);

        setOperationContext(tester, OperationType.GET);

        // test instance GET
        tester.testExecuteGetOperation(key, Arrays.asList(
                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(greetingBytes))));

        setOperationContext(tester, OperationType.UPDATE);

        // test instance partial UPDATE
        tester.testExecuteUpdateOperation(
                Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                   createGreeting(null, key, TEST_CONTENT2, null))),
                Arrays.<SimpleOperationResult>asList(
                        new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(keyBytes))));
        
        greetingBytes = createGreeting(timeStr, key, TEST_CONTENT2, TEST_AUTHOR);

        setOperationContext(tester, OperationType.GET);

        // check partial UPDATE result
        tester.testExecuteGetOperation(key, Arrays.asList(
                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(greetingBytes))));

        timeStr = currentTime();
        tester.setOperationContext(OperationType.UPDATE, null,
                                   Collections.<String,Object>singletonMap(DemoUpdateOperation.REPLACE_PROPERTY,
                                                                           Boolean.TRUE), TEST_TYPE, null);

        // test instance complete UPDATE (replacement)
        tester.testExecuteUpdateOperation(
                Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                   createGreeting(timeStr, key, null, null))),
                Arrays.<SimpleOperationResult>asList(
                        new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(keyBytes))));
        
        greetingBytes = createGreeting(timeStr, key, null, null);

        setOperationContext(tester, OperationType.GET);

        // check complete UPDATE result
        tester.testExecuteGetOperation(key, Arrays.asList(
                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(greetingBytes))));

        setOperationContext(tester, OperationType.DELETE);

        // test instance DELETE
        tester.testExecuteDeleteOperation(Arrays.asList(key), Arrays.asList(
                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.<byte[]>asList())));
        
        setOperationContext(tester, OperationType.GET);

        // check DELETE result (also test GET with unknown key)
        tester.testExecuteGetOperation(key, Arrays.asList(
                                               new SimpleOperationResult(OperationStatus.SUCCESS, "404", "Not Found", Arrays.<byte[]>asList())));
    }

    @Test
    public void testQueryOperation() throws Exception
    {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        String timeStr1 = currentTime();
        String testContent = TEST_CONTENT1 + timeStr1;

        setOperationContext(tester, OperationType.CREATE);

        // create some Greeting instances
        List<SimpleOperationResult> results =
            tester.executeCreateOperation(
                    Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                       createGreeting(timeStr1, null, testContent, TEST_AUTHOR))));

        SimpleOperationResult result1 = results.get(0);
        String key1 = getKey(result1);
        byte[] resultBytes1 = createGreeting(timeStr1, key1, testContent, TEST_AUTHOR, "UTF-8");
        
        String timeStr2 = currentTime();

        // make sure first and second times are distinct
        while(timeStr2.equals(timeStr1)) {
            Thread.sleep(1000);
            timeStr2 = currentTime();
        }

        results = tester.executeCreateOperation(
                Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                   createGreeting(timeStr2, null, testContent, TEST_AUTHOR))));

        SimpleOperationResult result2 = results.get(0);
        String key2 = getKey(result2);
        byte[] resultBytes2 = createGreeting(timeStr2, key2, testContent, TEST_AUTHOR, "UTF-8");
        
        setOperationContext(tester, OperationType.QUERY);

        // test QUERY for both instances
        tester.testExecuteQueryOperation(
                new QueryFilterBuilder(QueryGroupingBuilder.and(
                                               new QuerySimpleBuilder("content", "EQUALS", testContent),
                                               new QuerySimpleBuilder("date", "GREATER_THAN_OR_EQUALS", timeStr1)))
                .toFilter(),
                Arrays.asList(new SimpleOperationResult(OperationStatus.SUCCESS, "200", null, Arrays.asList(resultBytes1, resultBytes2),
                                                        true, false)));

        // test QUERY for second instance
        tester.testExecuteQueryOperation(
                new QueryFilterBuilder(QueryGroupingBuilder.and(
                                               new QuerySimpleBuilder("content", "EQUALS", testContent),
                                               new QuerySimpleBuilder("date", "EQUALS", timeStr2)))
                .toFilter(),
                Arrays.asList(new SimpleOperationResult(OperationStatus.SUCCESS, "200", null, Arrays.asList(resultBytes2))));

        // delete the test instances
        setOperationContext(tester, OperationType.DELETE);
        tester.executeDeleteOperation(Arrays.asList(key1, key2));
    }
    
    @Test
    public void testAppErrorOperation() throws Exception
    {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        // try creating a "bogus" type
        tester.setOperationContext(OperationType.CREATE, null, null, "BogusType", null);


        tester.testExecuteCreateOperation(
                Arrays.<InputStream>asList(new ByteArrayInputStream(
                                                   createGreeting(currentTime(), null, TEST_CONTENT1, TEST_AUTHOR))),
                                           Arrays.asList(
                                                   new SimpleOperationResult(OperationStatus.APPLICATION_ERROR, "404", "Not Found", null, false, true)));
    }

    @Test
    public void testFailedOperation() throws Exception
    {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        // try creating a "bogus" type
        tester.setOperationContext(OperationType.CREATE, null, null, TEST_TYPE, null);


        tester.testExecuteCreateOperation(
                Arrays.<InputStream>asList(new ByteArrayInputStream("This is not xml".getBytes(StringUtil.ASCII_CHARSET))),
                                           Arrays.asList(
                                                   new SimpleOperationResult(OperationStatus.FAILURE, "500", "Internal Server Error", null, false, true)));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testConstructQuery() throws Exception
    {
        // test simple expression
        assertEquals(new AbstractMap.SimpleEntry<String,String>("feq_content", "hello there"),
                     DemoQueryOperation.constructSimpleExpression(
                             new QuerySimpleBuilder("content", "EQUALS", "hello there").toExpression()));

        // test empty filters
        assertEquals(null, DemoQueryOperation.constructQueryTerms(null));
        assertEquals(null, DemoQueryOperation.constructQueryTerms(new QueryFilter()));

        // test filter with simple base expression
        assertEquals(Arrays.asList(new AbstractMap.SimpleEntry<String,String>("feq_content", "hello there")),
                     DemoQueryOperation.constructQueryTerms(
                             new QueryFilterBuilder(new QuerySimpleBuilder("content", "EQUALS", "hello there"))
                             .toFilter()));
        
        // test filter with grouping base expression
        assertEquals(Arrays.asList(new AbstractMap.SimpleEntry<String,String>("feq_content", "hello there"),
                                   new AbstractMap.SimpleEntry<String,String>("flt_range", "42")),
                     DemoQueryOperation.constructQueryTerms(
                             new QueryFilterBuilder(
                                     QueryGroupingBuilder.and(
                                             new QuerySimpleBuilder("content", "EQUALS", "hello there"),
                                             new QuerySimpleBuilder("range", "LESS_THAN", 42)))
                             .toFilter()));        
    }

    private static void setOperationContext(ConnectorTester tester, OperationType opType)
    {
        tester.setOperationContext(opType, null, null, TEST_TYPE, null);
    }

    private static String getKey(SimpleOperationResult result) throws Exception
    {
        Document doc = DBF.newDocumentBuilder().parse(new ByteArrayInputStream(result.getPayloads().get(0)));
        NodeList keys = doc.getElementsByTagName(KEY_EL_NAME);
        if(keys.getLength() != 1) {
            throw new IllegalStateException("Invalid key " + new String(result.getPayloads().get(0),
                                                                        StringUtil.UTF8_CHARSET));
        }
        return keys.item(0).getTextContent().trim();
    }

    private static byte[] createKeyResult(String key)
        throws Exception
    {
        Document doc = DBF.newDocumentBuilder().newDocument();
        doc.setXmlStandalone(true);

        addChild(doc, KEY_EL_NAME, key);
        return getDocBytes(doc, "utf-8");
    }
    
    private static byte[] createGreeting(String timeStr, String key, String contentStr,
                                         String authorStr)
        throws Exception
    {
        return createGreeting(timeStr, key, contentStr, authorStr, "utf-8");
    }
    
    private static byte[] createGreeting(String timeStr, String key, String contentStr,
                                         String authorStr, String encoding)
        throws Exception
    {
        Document doc = DBF.newDocumentBuilder().newDocument();
        doc.setXmlStandalone(true);

        Element greetingEl = doc.createElement(TEST_TYPE);
        doc.appendChild(greetingEl);

        addChild(greetingEl, KEY_EL_NAME, key);
        addChild(greetingEl, "content", contentStr);
        addChild(greetingEl, "date", timeStr);
        addChild(greetingEl, "author", authorStr);

        return getDocBytes(doc, encoding);
    }

    private static byte[] getDocBytes(Document doc, String encoding)
        throws Exception
    {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        Transformer transformer = XMLUtil.getTransformerFactory().newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
        transformer.transform(new DOMSource(doc), new StreamResult(bout));

        return bout.toByteArray();
    }

    private static void addChild(Node parent, String name, String content)
    {
        if(content != null) {
            Document doc = parent.getOwnerDocument();
            if(doc == null) {
                doc = (Document)parent;
            }
            Element el = doc.createElement(name);
            el.appendChild(doc.createTextNode(content));
            parent.appendChild(el);
        }
    }
    
    private static String currentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setTimeZone(GMT);
        return sdf.format(new Date());
    }
    
}
