// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.demo;

import org.junit.Test;

import com.boomi.connector.api.OperationType;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author James Ahlborn
 */
public class DemoBrowserTest {

    @Test
    public void testTypes() throws Exception {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        tester.setBrowseContext(OperationType.GET, null, null);

        tester.testBrowseTypes("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ObjectTypes"
                + "><type id=\"WholeEnchilada\"/><type id=\"DynoGreeting\"/><type id=\"Greeting\"/></ObjectTypes>");
    }

    @Test
    public void testProfile() throws Exception {
        DemoConnector connector = new DemoConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        tester.setBrowseContext(OperationType.GET, null, null);

        tester.testBrowseProfiles("Greeting",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ObjectDefinitions"
                        + "><definition elementName=\"Greeting\"><xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><xs:element name=\"Greeting\"><xs:complexType><xs:sequence><xs:element maxOccurs=\"1\" minOccurs=\"0\" name=\"key\" type=\"xs:normalizedString\"><xs:annotation><xs:appinfo><rest:metadata indexed=\"true\" required=\"true\" xmlns:rest=\"uri:com.boomi.rest\"/></xs:appinfo></xs:annotation></xs:element><xs:element maxOccurs=\"1\" minOccurs=\"0\" name=\"content\" type=\"xs:string\"><xs:annotation><xs:appinfo><rest:metadata indexed=\"true\" multiline=\"true\" required=\"false\" xmlns:rest=\"uri:com.boomi.rest\"/></xs:appinfo></xs:annotation></xs:element><xs:element maxOccurs=\"1\" minOccurs=\"0\" name=\"date\" type=\"xs:dateTime\"><xs:annotation><xs:appinfo><rest:metadata indexed=\"true\" required=\"false\" xmlns:rest=\"uri:com.boomi.rest\"/></xs:appinfo></xs:annotation></xs:element><xs:element maxOccurs=\"1\" minOccurs=\"0\" name=\"author\" type=\"xs:normalizedString\"><xs:annotation><xs:appinfo><rest:metadata indexed=\"true\" required=\"false\" xmlns:rest=\"uri:com.boomi.rest\"/></xs:appinfo></xs:annotation></xs:element></xs:sequence></xs:complexType></xs:element></xs:schema></definition></ObjectDefinitions>");
    }

}
