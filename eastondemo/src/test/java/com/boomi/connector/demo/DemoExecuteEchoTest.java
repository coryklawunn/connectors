package com.boomi.connector.demo;

import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.testutil.ConnectorTestContext;
import com.boomi.connector.testutil.SimpleOperationResult;
import com.boomi.connector.testutil.junit.ExecuteOperationTest;
import com.boomi.util.ClassUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;

public class DemoExecuteEchoTest extends ExecuteOperationTest{

    private static final String ECHO_EXPECTED_RESPONSE = "/EchoFields-expectedresult.json";

    public DemoExecuteEchoTest() {
        super("Echo", new ByteArrayInputStream("Blank".getBytes()));
    }

    @Override
    protected ConnectorTestContext getConnectorTestContext() {

        ConnectorTestContext testContext = new ConnectorTestContext() {
            @Override
            protected Class<? extends Connector> getConnectorClass() {
                return DemoConnector.class;
            }
        };

        testContext.addOperationProperty("integerField", 987654321L);
        testContext.addOperationProperty("stringField", "This is some random string");
        testContext.addOperationProperty("booleanField", true);
        testContext.addOperationProperty("passwordField", "pa$$w0000000rd");
        testContext.addOperationProperty("listField", "Greetings");
        testContext.addOperationProperty("radioField", 1L);
        testContext.addCookie(ObjectDefinitionRole.OUTPUT, new DemoEchoCookie("2019-03-12 12:43 +0000").getAsJson());
        testContext.setOperationCustomType("ECHO");
        testContext.setObjectTypeId("Echo");
        testContext.addOperationProperty("customPropertyOperationField", buildCustomPropertyMap());
        testContext.addConnectionProperty("customPropertyConnectionField", buildCustomPropertyMap());

        return testContext;
    }

    private Map<String, String> buildCustomPropertyMap() {
        Map<String, String> customPropertyMap = new HashMap<String, String>();
        customPropertyMap.put("demo", "property");
        return customPropertyMap;
    }


    @Override
    protected void afterOperation(SimpleOperationResult result) throws IOException {

        InputStream schemaIs = null;
        try{
            schemaIs = ClassUtil.getResourceAsStream(ECHO_EXPECTED_RESPONSE);
            assertArrayEquals(result.getPayloads().get(0), StreamUtil.toByteArray(schemaIs));
        } finally {
            IOUtil.closeQuietly(schemaIs);
        }
    }
}
